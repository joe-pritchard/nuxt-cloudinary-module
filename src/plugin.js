import Vue from 'vue';
import cloudinary from 'cloudinary-core';

export default ({ app }) => {
    const clCore = new cloudinary.Cloudinary(<%= JSON.stringify(options, null, 2) %>);

    Vue.prototype.$cl = clCore;
    app.$cl = clCore;
};
