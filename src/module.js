import path from 'path';

export default function SimpleModule(moduleOptions) {
    if (moduleOptions.cloud_name === undefined) {
        throw new Error('Please defined Cloudinary Cloud Name');
    }

    this.addPlugin({
        src: path.resolve(__dirname, 'plugin.js'),
        options: moduleOptions,
    });
}

module.exports.meta = require('../package.json');
