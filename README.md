# nuxt-cloudinary-module

Import the [Cloudinary core library](https://github.com/cloudinary/cloudinary-vue) into your Nuxt project as `this.$cl`

## Getting started

Install the dependency through your package manager:

```bash
npm install nuxt-cloudinary-module
```

or 
```bash
yarn add nuxt-cloudinary-module
```

Add it  to the `modules` section of your `nuxt.config.js`, passing in your Cloudinary "cloud name", 
and an optional value "secure" to indicate whether to always load urls as HTTPS 

```js
{
  modules: [
    ['nuxt-cloudinary-module', { cloud_name: process.env.CLOUDINARY_CLOUD_NAME, secure: true }],
  ]
}
```

### Typescript users
Add the types in `tsconfig.json`

```js
"types": [
  // ... //,
  "nuxt-cloudinary-module"
]
```
